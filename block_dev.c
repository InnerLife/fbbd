#include "block_dev.h"
#include "dbg.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <errno.h>

#include <sys/stat.h>
#include <unistd.h>

//including nul term
#define ULONG_MAX_STR_LEN 21 

block_dev_t* init_block_dev(block_dev_t *bd, uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek){
	bd->block_size = block_size;
	bd->blocks_count = blocks_count;
	bd->use_seek = use_seek;
	bd->dir_len = strlen(dir);
	if (bd->dir_len + 2/*/b*/ + ULONG_MAX_STR_LEN > PATH_MAX){
		warnx("Dir length is too long.");
		return NULL;
	}
	strncpy(bd->dir, dir, PATH_MAX - 1);
	strcat(bd->dir, "/b");
	bd->dir_len = bd->dir_len + 2;
	bd->buf = malloc(block_size);
	if (bd->buf == NULL){
		warnx("Error allocating block device buffer.");
		return NULL;
	}	
	dbg("Block device initialized.");
	return bd;
}

block_dev_t* alloc_block_dev(uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek){
	block_dev_t *bd = malloc(sizeof(*bd));
	if (bd == NULL){
		warnx("Error allocating block device");
		return NULL;
	}
	return init_block_dev(bd, block_size, blocks_count, dir, use_seek);
}

void free_block_dev_data(block_dev_t *bd){
	free(bd->buf);
}

void free_block_dev(block_dev_t *bd){
	free_block_dev_data(bd);
	free(bd);
}

static char* get_file_name(block_dev_t *bd, uint32_t block_num){
	static char uint_str[ULONG_MAX_STR_LEN];
	sprintf(uint_str, "%d", block_num);
	memcpy(bd->dir + bd->dir_len, uint_str, ULONG_MAX_STR_LEN);
	return bd->dir;
}
static int file_seek(FILE *file, off_t offset){
	if (offset == 0){
		return 0;
	}
	if (fseek(file, offset, SEEK_SET) < 0) {
		warn("Error seeking block file.");
		return -1;
	}
	dbg("File seeked using seek(). %ld", offset);
	return offset;
}

static int file_seek_using_read(block_dev_t *bd, FILE *file, off_t offset){
	if (offset == 0){
		return 0;
	}
	int bytes_read = fread(bd->buf, 1, offset, file);
	if (bytes_read < 0 || (bytes_read == 0 && !feof(file))){
		warn("Error seeking block file using read.");
		return -1;
	}
	if (bytes_read != offset){
		warn("Error seeking block file using read. Offset and bytes_read not matched. %ld %d", offset, bytes_read);
		return -1;
	}
	/*
	for (int i = 0; i < offset; i++){
		char b[1];
		bytes_read = fread(b, 1, sizeof(b), file);
		if (bytes_read < 0 || bytes_read == 0 && !feof(file)){
			warn("Error seeking block file using read. %d", i);
			return -1;
		}
	}
	bytes_read = offset;
	*/
	dbg("File seeked using read(). %ld %d", offset, bytes_read);
	return bytes_read;
}

static void* file_read(FILE *file, void* buf, size_t count){
	size_t bytes_read = fread(buf, 1, count, file);
	if (bytes_read > 0){
		dbg("%lu bytes read from file.", bytes_read);
		if (bytes_read != count){
			dbg("Filling others with zeroes... %ld", bytes_read);
			memset(buf + bytes_read, 0, count - bytes_read);
		}
		return buf;
	}
	if (feof(file)){
		dbg("File is empty. Filling buffer with zeroes...");
		memset(buf, 0, count);
		return buf;
	}
	warn("Error reading from block file.");
	return NULL;
}

static void* file_write(FILE *file, void* buf, size_t count){
	int bytes_written = fwrite(buf, 1, count, file);
	if (bytes_written == count) {
		dbg("%d bytes written to file.", bytes_written);
		return buf;
	}
	warn("Error writing to block file.");
	return NULL;
}

static void* block_read(block_dev_t *bd, void* buf, size_t count, off_t offset, int file_size){
	if (file_size <= offset){
		dbg("File is empty or size is less than offset. Filling buffer with zeroes...");
		memset(buf, 0, count);
		return buf;
	}
	FILE *file = fopen(bd->dir, "r");
	if (file == NULL){
		warn("Error opening block file. %s", bd->dir);
		return NULL;
	} 
	void *res = NULL;
	if (bd->use_seek && (file_seek(file, offset) == offset || file_seek_using_read(bd, file, offset) == offset)){
		res = file_read(file, buf, count);
	}
	fclose(file);
	return res;
}

static void* block_write(block_dev_t *bd, void* buf, size_t count, off_t offset, int file_size){
	FILE *file = NULL;
	if (file_size <= 0){
		file = fopen(bd->dir, "w");
		if (file == NULL){
			warn("Error creating/rewriting empty block file. %s", bd->dir);
			return NULL;
		}
		if (offset > 0){
			memset(bd->buf, 0, offset);
			if (file_write(file, bd->buf, offset) == NULL){
				warn("Error writing zeroes to empty block file.");
				return NULL;
			}
		}
		void *res = file_write(file, buf, count);
		fclose(file);
		return res;
	}
	//file_size > 0
	if (bd->use_seek){
		file = fopen(bd->dir, "r+");
		if (file == NULL){
			warn("Error opening block file. %s", bd->dir);
			return NULL;
		}
		void *res = NULL;
		if (file_seek(file, offset) == offset){
			res = file_write(file, buf, count);
		}
		fclose(file);
		return res;
	} else {
		file = fopen(bd->dir, "r");
		if (file == NULL){
			warn("Error opening block file. %s", bd->dir);
			return NULL;
		}
		void *res = file_read(file, bd->buf, file_size);
		fclose(file);
		if (res == NULL){
			return NULL;
		}
		int zeroes_count = offset - file_size;
		if (zeroes_count > 0){
			dbg("Filling buffer with zeroes to offset... %ld %d", offset, file_size);
			memset(bd->buf + file_size, 0, zeroes_count);
			file_size += zeroes_count;
		}
		memcpy(bd->buf + offset, buf, count);
		if (file_size < offset + count){
			file_size = offset + count;
		}
		file = fopen(bd->dir, "w");
		if (file == NULL){
			warn("Error opening block file. %s", bd->dir);
			return NULL;
		}
		res = file_write(file, bd->buf, file_size);
		fclose(file);
		return res;
	}
}

static void* process_block(block_dev_t *bd, void *buf, uint64_t from, uint32_t len, int read_or_write){
	uint32_t num_first = from / bd->block_size;//first block to process
	uint32_t num_last = (from + len - 1) / bd->block_size;//last block to process
	dbg("Block nums calculated. %u %u", num_first, num_last);
	if (num_first > bd->blocks_count || num_last > bd->blocks_count){
		warn("Trying to request block which is out of range.");
		return NULL;
	}
	
	off_t offset = from % bd->block_size;//offset in current block
	uint32_t count;//number of bytes to process in current block
	for (uint32_t b = num_first; b <= num_last; b++){
		count = bd->block_size - offset;
		if (len < count){
			count = len;
		}
		get_file_name(bd, b);
		dbg("Processing block %s. count: %d. offset: %ld.", bd->dir, count, offset);
		struct stat st;
		int file_size;
		if (stat(bd->dir, &st) < 0){
			if (errno == ENOENT){
				file_size = -1;
			} else {
				warn("Error gathering info about a file. %s", bd->dir);
				return NULL;
			}
		} else {
			file_size = st.st_size;
		}
		void *res;
		if (read_or_write){
			res = block_write(bd, buf, count, offset, file_size);
		} else {
			res = block_read(bd, buf, count, offset, file_size);
		}
		if (res == NULL){
			return NULL;
		}
		len -= count; //bytes left
		buf += count;
		offset = 0;
	}
	return bd;
}

void* bd_read(block_dev_t *bd, void *buf, uint64_t from, uint32_t len){
	return process_block(bd, buf, from, len, 0);
}

void* bd_write(block_dev_t *bd, void *buf, uint64_t from, uint32_t len){
	return process_block(bd, buf, from, len, 1);
}