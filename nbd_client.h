/** @file nbd_client.h
 *  @brief NBD client
 */
#ifndef H_NBD_CLIENT
#define H_NBD_CLIENT

#include "nbd_server.h"
#include <stdint.h>
#include <limits.h>

typedef struct nbd_client {
	char nbd_dev[PATH_MAX]; /**< path to NBD dev */
	int nbd_fd; /**< NBD fd */
	int fd; /**< socket FD */
	nbd_server_t *server; /**< ptr to server */
	uint32_t sector_size; /**< NBD dev sector size */
	uint32_t timeout; /**< NBD client timeout, NBD_SET_TIMEOUT */
} nbd_client_t;

/**
 * @brief Initialize already allocated NBD client
 * 
 * @param client NBD client
 * @param fd above
 * @param server above
 * @param nbd_dev above
 * @param sector_size above
 * @param timeout above
 * @return NULL on error, otherwise - success
 */
nbd_client_t* init_nbd_client(nbd_client_t *client, int fd, nbd_server_t *server, char *nbd_dev, uint32_t sector_size, uint32_t timeout);
/**
 * @brief Allocate and initialize NBD client
 * 
 * @param fd above
 * @param server above
 * @param nbd_dev above
 * @param sector_size above
 * @param timeout above
 * @return NULL on error, otherwise - success
 */
nbd_client_t* alloc_nbd_client(int fd, nbd_server_t *server, char *nbd_dev, uint32_t sector_size, uint32_t timeout);
/**
 * @brief Free NBD client internal data structures
 * 
 * @param client NBD client
 */
void free_nbd_client_data(nbd_client_t *client);
/**
 * @brief Free NBD client internal data structures and ptr itself
 * 
 * @param client NBD client
 */
void free_nbd_client(nbd_client_t *client);
/**
 * @brief Open and preapre virtual NBD dev for use
 * 
 * @param client NBD client
 * @return NULL on error, otherwise - success
 */
nbd_client_t* nbd_client_open(nbd_client_t *client);
/**
 * @brief Run NBD client. Wait until stop.
 * 
 * @param client NBD client
 * @return NULL on error, otherwise - success
 */
nbd_client_t* nbd_client_run(nbd_client_t *client);
/**
 * @brief Close NBD client nbd_fd.
 * 
 * @param client NBD client
 */
void nbd_client_close(nbd_client_t *client);

#endif