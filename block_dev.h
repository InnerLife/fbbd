/** @file block_dev.h
 *  @brief Virtual file-backed block device
 */
#ifndef H_BLOCK_DEV
#define H_BLOCK_DEV

#include <stdint.h>
#include <limits.h>
#include <stddef.h>

typedef struct block_dev {
	uint32_t block_size; /**< block size, block file max size */
	uint32_t blocks_count; /**< blocks count */
	char dir[PATH_MAX]; /**< directory contains block files */
	size_t dir_len; /**< strlen() of dir, for block file name calculation optimization */
	int use_seek; /**< use seek() or seek block files through read() */
	void *buf; /**< universal purpose buffer with size of block_size */
} block_dev_t;

/**
 * @brief Initialize already allocated block_dev
 * 
 * @param bd block device
 * @param block_size above
 * @param blocks_count above
 * @param dir above
 * @param use_seek above
 * @return NULL on error, otherwise - success
 */
block_dev_t* init_block_dev(block_dev_t *bd, uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek);
/**
 * @brief Allocate and initialize block_dev
 * 
 * @param block_size above
 * @param blocks_count above
 * @param dir above
 * @param use_seek above
 * @return NULL on error, otherwise - success
 */
block_dev_t* alloc_block_dev(uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek);
/**
 * @brief Free block device internal data structures
 * 
 * @param bd block device
 */
void free_block_dev_data(block_dev_t *bd);
/**
 * @brief Free block device internal data structures and ptr itself
 * 
 * @param bd block device
 */
void free_block_dev(block_dev_t *bd);
/**
 * @brief Read from block device
 * 
 * @param bd block device
 * @param buf where to read
 * @param from block device offset
 * @param len how much to read
 * @return NULL on error, otherwise - success
 */
void* bd_read(block_dev_t *bd, void *buf, uint64_t from, uint32_t len);
/**
 * @brief Write to block device
 * 
 * @param bd block device
 * @param buf what to write
 * @param from block device offset
 * @param len how much to write
 * @return NULL on error, otherwise - success
 */
void* bd_write(block_dev_t *bd, void *buf, uint64_t from, uint32_t len);

#endif