# FBBD
File-backed block device. FBBD creates a block device out of multiple files. Each file mimics one block.
FBBD acts like a mdadm RAID0 drive, but without a limit to underlying files count. Underlying block files creating automatically on-the-fly, so it's a thin-provisioning device.
Like [BUSE](https://github.com/acozzette/BUSE), FBBD use NBD local sockets client-server model, but has it's own codebase.

### Usage
Compile
```sh
root@test:~# make
```
Examine
```sh
root@test:~# ./fbbd -h
Usage: ./fbbd [options]
-n <uint> Number of blocks
-p <path> Block files directory
-b <uint> Block file size. Default: 4096 (bytes).
-r <uint> Sector size of virtual /dev/nbd device. Default: 4096 (bytes).
-d <path> NBD device path. Default: /dev/nbd0.
-s        Don`t use seek() to access a part of block file.
-t <uint> NBD client timeout. Default: 30 (seconds).
```
Run.
Example: 100MiB (1MiB * 100 blocks) /dev/nbd1 device with block files stored in /mnt/blocks/ directory.
```sh
root@test:~# modprobe nbd
root@test:~# ./fbbd -b 1048576 -n 100 -p /mnt/blocks/ -d /dev/nbd1 
```
Use
```sh
root@test:~# mkfs.ext4 /dev/nbd1 #or whatever
```
