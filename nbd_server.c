#include "nbd_server.h"
#include "dbg.h"

#include <err.h>
#include <unistd.h>
#include <linux/nbd.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

nbd_server_t* init_nbd_server(nbd_server_t *server, int fd, uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek){
	server->fd = fd;
	if (init_block_dev(&server->bd, block_size, blocks_count, dir, use_seek) == NULL){
		return NULL;
	}
	dbg("NBD server initialized.");
	return server;
}

nbd_server_t* alloc_nbd_server(int fd, uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek){
	nbd_server_t *server = malloc(sizeof(*server));
	if (server == NULL){
		warnx("Error allocating NBD server");
		return NULL;
	}
	return init_nbd_server(server, fd, block_size, blocks_count, dir, use_seek);
}

void free_nbd_server_data(nbd_server_t *server){
	free_block_dev_data(&server->bd);
}

void free_nbd_server(nbd_server_t *server){
	free_nbd_server_data(server);
	free(server);
}

static u_int64_t ntohll(u_int64_t a) {
	#ifdef WORDS_BIGENDIAN
		return a;
	#else
		u_int32_t lo = a & 0xffffffff;
		u_int32_t hi = a >> 32U;
		lo = ntohl(lo);
		hi = ntohl(hi);
		return ((u_int64_t) lo) << 32U | hi;
	#endif
}

static void* fd_read_all(int fd, char* buf, size_t count){
	int bytes_read;
	while (count > 0) {
		bytes_read = read(fd, buf, count);
		if (bytes_read <= 0){
			warn("Error reading NBD server socket.");
			return NULL;
		}
		buf += bytes_read;
		count -= bytes_read;
	}
	return buf;
}

static void* fd_write_all(int fd, char* buf, size_t count){
	int bytes_written;
	while (count > 0) {
		bytes_written = write(fd, buf, count);
		if (bytes_written <= 0){
			warn("Error writing to NBD server socket.");
			return NULL;
		}
		buf += bytes_written;
		count -= bytes_written;
	}
	return buf;
}

static nbd_server_t* process_read(nbd_server_t *server, struct nbd_reply *reply, void *buf, uint64_t from, uint32_t len){
	if (bd_read(&server->bd, buf, from, len) == NULL){
		reply->error = htonl(EPERM);
	}
	if (fd_write_all(server->fd, (void*)reply, sizeof(struct nbd_reply)) <= 0){
		return NULL;
	}
	if (fd_write_all(server->fd, buf, len) <= 0){
		return NULL;
	}
	if (reply->error != 0){
		return NULL;
	}
	return server;
}

static nbd_server_t* process_write(nbd_server_t *server, struct nbd_reply *reply, void *buf, uint64_t from, uint32_t len){
	if (fd_read_all(server->fd, buf, len) <= 0){
		return NULL;
	}
	if (bd_write(&server->bd, buf, from, len) == NULL){
		reply->error = htonl(EPERM);
	}
	if (fd_write_all(server->fd, (void*)reply, sizeof(struct nbd_reply)) <= 0){
		return NULL;
	}
	if (reply->error != 0){
		return NULL;
	}
	return server;
}

nbd_server_t* nbd_server_run(nbd_server_t *server){
	uint64_t from;
	uint32_t len;
	uint32_t type;
	size_t bytes_read;
	struct nbd_request request;
	struct nbd_reply reply;
	void *buf;
	nbd_server_t *res;
	
	reply.magic = htonl(NBD_REPLY_MAGIC);
	
	while (server->fd != -1 && (bytes_read = read(server->fd, &request, sizeof(request))) > 0) {
		if (htonl(NBD_REQUEST_MAGIC) != request.magic){
			warn("Client request magic is wrong.");
			return NULL;
		}	
		len = ntohl(request.len);
		from = ntohll(request.from);
		type = ntohl(request.type);
		dbg("Got request %u. Len: %u. From: %lu.", type, len, from);
		
		memcpy(reply.handle, request.handle, sizeof(reply.handle));
		reply.error = htonl(0);

		if (type == NBD_CMD_DISC){
			dbg("Got NBD_CMD_DISC. Stopping server...");
			return server;
		}

		buf = malloc(len);
		if (buf == NULL){
			warn("Error allocating socket buffer.");
			return NULL;
		}
		switch(type) {
			case NBD_CMD_READ:
				res = process_read(server, &reply, buf, from, len);
				break;
			case NBD_CMD_WRITE:
				res = process_write(server, &reply, buf, from, len);
				break;
		}		
		free(buf);
		if (res == NULL){
			return NULL;
		}
	}
	if (bytes_read < 0){
		warn("NBD server socket read error.");
		return NULL;
	}
	return server;
}

void nbd_server_stop(nbd_server_t *server){
	if (server->fd > 0){
		shutdown(server->fd, SHUT_RDWR);
		dbg("NBD server shutted down.");
	}
}

void nbd_server_close(nbd_server_t *server){
	if (close(server->fd) < 0){
		warn("NBD server error. Error closing server socket.");
	} else {
		dbg("NBD server closed.");
		server->fd = -1;
	}
}
/*
nbd_server_t* nbd_server_init_dir(nbd_server_t *server){
	for (uint32_t b = 0; b < server->blocks_count; b++){
		gen_file_name(server, b);
		FILE *file = fopen(server->dir, "a+b");
		if (file == NULL){
			warn("Error initializing block: %s", server->dir);
			return NULL;
		}
		fclose(file);
		printf("Block initialized: %s. %d blocks left.\n", server->dir, (server->blocks_count - b - 1));
	}
	return server;
}
*/