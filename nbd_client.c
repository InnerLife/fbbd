#include "nbd_client.h"
#include "dbg.h"

#include <linux/nbd.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <err.h>
#include <stdio.h>
#include <string.h>

nbd_client_t* init_nbd_client(nbd_client_t *client, int fd, nbd_server_t *server, char *nbd_dev, uint32_t sector_size, uint32_t timeout){
	client->fd = fd;
	client->server = server;
	client->sector_size = sector_size;
	client->timeout = timeout;
	int nbd_dev_len = strlen(nbd_dev);
	if (nbd_dev_len + 1/*nul term*/ > PATH_MAX){
		warnx("NBD dev length is too long.");
		return NULL;
	}
	strncpy(client->nbd_dev, nbd_dev, PATH_MAX - 1);
	dbg("NBD client initialized.");
	return client;
}

nbd_client_t* alloc_nbd_client(int fd, nbd_server_t *server, char *nbd_dev, uint32_t sector_size, uint32_t timeout){
	nbd_client_t *client = malloc(sizeof(*client));
	if (client == NULL){
		warnx("Error allocating NBD client");
		return NULL;
	}
	return init_nbd_client(client, fd, server, nbd_dev, sector_size, timeout);
}

void free_nbd_client_data(nbd_client_t *client){
	
}

void free_nbd_client(nbd_client_t *client){
	free_nbd_client_data(client);
	free(client);
}

nbd_client_t* nbd_client_open(nbd_client_t *client){
	client->nbd_fd = open(client->nbd_dev, O_RDWR);
	if (client->nbd_fd < 0){
		warn("NBD client error. Error opening NBD device.");
		return NULL;
	}
	if (ioctl(client->nbd_fd, NBD_SET_BLKSIZE, client->sector_size) < 0){
		warn("NBD client error. Error setting sector size.");
		return NULL;
	}
	if (ioctl(client->nbd_fd, NBD_SET_SIZE_BLOCKS, client->server->bd.blocks_count * client->server->bd.block_size / client->sector_size) < 0){
		warn("NBD client error. Error setting sectors count.");
		return NULL;
	}
	if (ioctl(client->nbd_fd, NBD_CLEAR_SOCK) < 0){
		warn("NBD client error. Error clearing NBD socket.");
		return NULL;
	}
	if (ioctl(client->nbd_fd, NBD_SET_SOCK, client->fd) < 0){
		warn("NBD client error. Error setting NBD socket.");
		return NULL;
	}
	
	if (ioctl(client->nbd_fd, NBD_SET_TIMEOUT, client->timeout) < 0){
		warn("NBD client error. Error setting nbd timeout.");
		return NULL;
	}
	dbg("NBD client started.");
	return client;
}

nbd_client_t* nbd_client_run(nbd_client_t *client){
	if (ioctl(client->nbd_fd, NBD_DO_IT) < 0){
		warn("NBD client terminated.");
		return NULL;
	}
	dbg("NBD client stopped.");
	return client;
}

void nbd_client_close(nbd_client_t *client){
	int nbd_fd = client->nbd_fd;	
	if (nbd_fd != -1){
		if (ioctl(nbd_fd, NBD_DISCONNECT) < 0){
			warn("NBD client error. Error disconnecting NBD device.");
		} else {
			client->nbd_fd = -1;
		}
		if (ioctl(nbd_fd, NBD_CLEAR_QUE) < 0 || ioctl(nbd_fd, NBD_CLEAR_SOCK) < 0){
			warn("NBD client error. Error peforming NBD clean up.");
		}
		dbg("NBD client closed.");
	}
}