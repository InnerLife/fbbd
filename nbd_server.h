/** @file nbd_server.h
 *  @brief NBD server
 */
#ifndef H_NBD_SERVER
#define H_NBD_SERVER

#include "block_dev.h"

typedef struct nbd_server {
	int fd; /**< socket fd */
	block_dev_t bd; /**< block device */
} nbd_server_t;

/**
 * @brief Initialize already allocated NBD server
 * 
 * @param server NBD server
 * @param block_size to underlying block_dev
 * @param blocks_count to underlying block_dev
 * @param dir to underlying block_dev
 * @param use_seek to underlying block_dev
 * @return NULL on error, otherwise - success
 */
nbd_server_t* init_nbd_server(nbd_server_t *server, int fd, uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek);
/**
 * @brief Allocate and initialize NBD server
 * 
 * @param block_size to underlying block_dev
 * @param blocks_count to underlying block_dev
 * @param dir to underlying block_dev
 * @param use_seek to underlying block_dev
 * @return NULL on error, otherwise - success
 */
nbd_server_t* alloc_nbd_server(int fd, uint32_t block_size, uint32_t blocks_count, char *dir, int use_seek);
/**
 * @brief Free NBD server internal data structures
 * 
 * @param server NBD server
 */
void free_nbd_server_data(nbd_server_t *server);
/**
 * @brief Free NBD server internal data structures and ptr itself
 * 
 * @param server NBD server
 */
void free_nbd_server(nbd_server_t *server);
/**
 * @brief Run NBD server. Listen on socket in loop until stop.
 * 
 * @param server NBD server
 * @return NULL on error, otherwise - success
 */
nbd_server_t* nbd_server_run(nbd_server_t *server);
/**
 * @brief Stop running NBD server.
 * 
 * @param server NBD server
 */
void nbd_server_stop(nbd_server_t *server);
/**
 * @brief Close NBD server.
 * 
 * @param server NBD server
 */
void nbd_server_close(nbd_server_t *server);

#endif