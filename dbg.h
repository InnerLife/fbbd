#ifndef H_DBG
#define H_DBG

#ifndef FBBD_DEBUG
	#define FBBD_DEBUG 0
#endif

#define dbg(format, ...) do {\
	if (FBBD_DEBUG) {\
		printf(format"\n", ##__VA_ARGS__);\
	}\
} while (0)

#endif