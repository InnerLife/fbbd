#include "nbd_server.h"
#include "nbd_client.h"

#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <sys/wait.h>
#include <err.h>
#include <stdlib.h>

#include <fcntl.h>
#include "dbg.h"

static uint32_t DEFAULT_SECTOR_SIZE = 4096;
static char DEFAULT_NBD_DEV[] = "/dev/nbd0";
static uint32_t DEFAULT_CLIENT_TIMEOUT = 30;

static nbd_server_t server;
static nbd_client_t client;

struct params {
	uint32_t block_size;
	uint32_t blocks_count;
	char* nbd_dev;
	char* dir;
	int dont_seek;
	uint32_t sector_size;
	uint32_t timeout;
};

int check_args(int argc, char **argv, struct params *params){
	int c;
	while ((c = getopt (argc, argv, "sn:p:b:r:d:t:h")) != -1) {
		switch (c) {
			case 's':
				params->dont_seek = 1;
				break;
			case 'n':
				params->blocks_count = atoi(optarg);
				break;
			case 'p':
				params->dir = optarg;
				break;				
			case 'b':
				params->block_size = atoi(optarg);
				break;
			case 'r':
				params->sector_size = atoi(optarg);
				break;
			case 'd':
				params->nbd_dev = optarg;
				break;
			case 't':
				params->timeout = atoi(optarg);
				break;
			case 'h':
				//to default
			default:
				fprintf(stderr, "Usage: %s [options]\n", argv[0]);
				fprintf(stderr, "-n <uint> Number of blocks\n");
				fprintf(stderr, "-p <path> Block files directory\n");
				fprintf(stderr, "-b <uint> Block file size. Default: 4096 (bytes).\n");
				fprintf(stderr, "-r <uint> Sector size of virtual /dev/nbd device. Default: 4096 (bytes).\n");
				fprintf(stderr, "-d <path> NBD device path. Default: /dev/nbd0.\n");
				fprintf(stderr, "-s        Don't use seek() to access a part of block file.\n");
				fprintf(stderr, "-t <uint> NBD client timeout. Default: 30 (seconds).\n");
				return EXIT_FAILURE;
		}
	}
	if (params->nbd_dev == 0){
		params->nbd_dev = DEFAULT_NBD_DEV;
		printf("Using default NBD dev: %s\n", DEFAULT_NBD_DEV);
	}
	if (params->timeout == 0){
		params->timeout = DEFAULT_CLIENT_TIMEOUT;
		printf("Using default NBD client timeout: %u\n", DEFAULT_CLIENT_TIMEOUT);
	}
	if (params->sector_size <= 0){
		params->sector_size = DEFAULT_SECTOR_SIZE;
		printf("Using default block size: %u\n", DEFAULT_SECTOR_SIZE);
	} else {
		if ((params->sector_size & (params->sector_size - 1)) != 0){
			warnx("Sector size should be a power of 2.");
			return EXIT_FAILURE;
		}
	}
	if (params->block_size <= 0){
		params->block_size = params->sector_size;
		printf("Using block size equals to sector size: %u\n", params->sector_size);
	} else {
		if (params->sector_size > params->block_size){
			warnx("Block size should be greater or equal to sector size.");
			return EXIT_FAILURE;
		}
		if ((params->block_size & (params->block_size - 1)) != 0){
			warnx("Block size should be a power of 2.");
			return EXIT_FAILURE;
		}
	}
	if (params->blocks_count <= 0){
		warnx("Blocks count param can not be empty.");
		return EXIT_FAILURE;
	}
	if (params->dir == 0){
		warnx("Blocks directory param can not be empty.");
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

static void stop_server(){
	nbd_client_close(&client);
	nbd_server_stop(&server);
}

static void signal_server(int signum){
	stop_server();
}

static int process_client(){
	signal(SIGINT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	
	if (nbd_client_run(&client) == NULL){
		nbd_server_stop(&server);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

static int wait_child(pid_t child_pid){
	int status;
	if (waitpid(child_pid, &status, 0) < 0){
		return EXIT_FAILURE;
	}
	if (WIFEXITED(status)){
		return WEXITSTATUS(status);
	}
	return EXIT_SUCCESS;
}

static int process_server(pid_t child_pid){
	signal(SIGINT, signal_server);
	signal(SIGTERM, signal_server);
	/*struct sigaction act;
	act.sa_handler = signal_server;
	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGINT);
    sigaddset(&act.sa_mask, SIGTERM);*/
	
	nbd_server_t *run_res = nbd_server_run(&server);
	if (run_res == NULL){
		stop_server();
	}
	nbd_server_close(&server);
	
	int wait_res = wait_child(child_pid);
	if (run_res == NULL){
		return EXIT_FAILURE;
	}	
	return wait_res;
}

int main(int argc, char **argv){
	struct params params = {0};
	if (check_args(argc, argv, &params) == EXIT_FAILURE){
		return EXIT_FAILURE;
	}
	int sp[2];
	if (socketpair(AF_UNIX, SOCK_STREAM, 0, sp) < 0){
		warn("Error creating NBD socket pair.");
		return EXIT_FAILURE;
	}

	if (init_nbd_server(&server, sp[0], params.block_size, params.blocks_count, params.dir, !params.dont_seek) == NULL){
		return EXIT_FAILURE;
	}
	if (init_nbd_client(&client, sp[1], &server, params.nbd_dev, params.sector_size, params.timeout) == NULL){
		free_nbd_server_data(&server);
		return EXIT_FAILURE;
	}
	if (nbd_client_open(&client) == NULL) {
		free_nbd_server_data(&server);
		free_nbd_client_data(&client);
		return EXIT_FAILURE;
	}

	int res;
	pid_t pid = fork();
	if (pid == 0) {
		close(sp[0]);
		res = process_client();
	} else {
		close(sp[1]);
		res = process_server(pid);
		free_nbd_client_data(&client);
		free_nbd_server_data(&server);
	}
	return res;
}