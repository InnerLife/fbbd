CC=gcc
override CFLAGS += -O3 -Wall

compile:
	$(CC) $(CFLAGS) main.c nbd_server.c nbd_client.c block_dev.c ${ARGS} -o fbbd
	
compile_debug:
	$(MAKE) ARGS="-D FBBD_DEBUG=1" compile